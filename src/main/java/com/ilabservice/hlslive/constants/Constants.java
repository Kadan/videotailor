package com.ilabservice.hlslive.constants;

public class Constants {

    public static String firstFrameImgName = "firstFrame.jpg";
    public static  String tailorVideoPrefix = "-ted";
    public static  String tailorIndexFile = "index.txt";

    public static String event_MEASUREMENT = "EVENT_MEASUREMENTS";
    public static String rtmp_MEASUREMENT = "RTMP_MEASUREMENTS";
    public static String darwin_EVENT_MEASUREMENT = "DARWIN_EVENT_MEASUREMENTS";
    public static String error_MEASUREMENT = "ERROR_MEASUREMENTS";
    public static String recorder_MEASUREMENT = "RECORDER_MEASUREMENTS";

    public static String CREATED = "CREATED";
    public static String DELETED = "DELETED";

    public static String VIDEO_TYPE_EVT = "event";
}
