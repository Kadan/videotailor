package com.ilabservice.hlslive.fileupload;



import com.ilabservice.hlslive.config.MQTTConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class MqttTestController {

    @Autowired
    private MQTTConfiguration.MqttGateway mqttGateway;

    @GetMapping("/mqtt/send/{message}")
    public String sendMqtt(HttpServletRequest request, @PathVariable String message){

        //mqttGateway.sendMessage("hello tomorrow");
        mqttGateway.sendToMqtt("EVT_VIDEO_DETECTED","hello tomorrow " + message);

        return "OK";
    }

}
