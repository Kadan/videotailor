package com.ilabservice.hlslive.fileupload;

import com.alibaba.fastjson.JSONObject;
import com.ilabservice.hlslive.config.InfluxDbUtils;
import com.ilabservice.hlslive.constants.Constants;
import com.ilabservice.hlslive.entity.RetMsg;
import com.ilabservice.hlslive.ffmpeg.FileTailor;
import com.ilabservice.hlslive.ffmpeg.FirstFrame;
import com.ilabservice.hlslive.measurements.ErrorMeasurement;
import com.ilabservice.hlslive.measurements.EventMeasurement;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFprobe;
import org.apache.commons.lang3.StringUtils;
import org.influxdb.dto.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/upload")
@Api(value = "event videos upload")
public class FileUploadController {

    @Value("${fileUploadPath}")
    private String fileUploadPath;


    @Value("${fileDownloadPath}")
    private String fileDownloadPath;

    @Value("${ffmpegpath}")
    private String ffmpegpath;

    @Autowired
    FileTailor fileTailor;

    @Autowired
    FirstFrame firstFrame;

    @Autowired
    InfluxDbUtils influxDbUtils;

    private static final Logger log = LoggerFactory.getLogger(FileUploadController.class);

//    @RequestMapping(value="upload",method = RequestMethod.GET)
//    public String index() {
//        return "upload";
//    }


//    @PostMapping("/uploadSingle")
//    @ResponseBody
//    public Map<String, String> upload1(@RequestParam("file") MultipartFile file) throws IOException {
//        log.info("[文件类型] - [{}]", file.getContentType());
//        log.info("[文件名称] - [{}]", file.getOriginalFilename());
//        log.info("[文件大小] - [{}]", file.getSize());
//        // write the file
//        file.transferTo(new File(fileUploadPath + file.getOriginalFilename()));
//        Map<String, String> result = new HashMap<>(16);
//        result.put("contentType", file.getContentType());
//        result.put("fileName", file.getOriginalFilename());
//        result.put("fileSize", file.getSize() + "");
//        return result;
//    }

    @PostMapping("/evtvideos/{cameraId}")
    //@ResponseBody
    @ApiOperation(value = "upload videos", notes = "distinct by cameraId, event startTime, endTime, headOffset and tailOffset are mandatory")
    public ResponseEntity uploadMoreFile(HttpServletRequest request, @RequestParam(value = "startTime", required = true) int startTime,
                                         @RequestParam(value = "endTime", required = true) int endTime,
                                         @RequestParam(value = "headOffset", required = true) int headOffset,
                                         @RequestParam(value = "tailOffset", required = true) int tailOffset,
                                         @RequestParam("files") MultipartFile[] multipartFiles, @PathVariable String cameraId) throws IllegalStateException {


        if (StringUtils.isEmpty(cameraId))
            //return new ResponseEntity<String>("camera id can not be empty", HttpStatus.BAD_REQUEST);
            return new ResponseEntity(new RetMsg(2, "camera id could not be null!!"), HttpStatus.BAD_REQUEST);

        if (startTime >= endTime)
            return new ResponseEntity(new RetMsg(1, "start time can not be earlier than end time"), HttpStatus.BAD_REQUEST);

        String uploadPath = fileUploadPath.concat(cameraId);
        //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");

        String subFolder = "";
        String destinationFile = "";
        if(String.valueOf(startTime).length()==10 && String.valueOf(endTime).length()==10){
            destinationFile = startTime + "000-" + endTime + "999.mp4";
            subFolder = String.valueOf(startTime).concat("000-").concat(String.valueOf(endTime)).concat("999");
        }
        else{
            destinationFile = startTime + "-" + endTime + ".mp4";
            subFolder = String.valueOf(startTime).concat("-").concat(String.valueOf(endTime));
        }

        File destinationFolder = new File(uploadPath);
        //String destinationFile = simpleDateFormat.format(new Date(startTime * 1000L)) + "-" + simpleDateFormat.format(new Date(endTime * 1000L+999)) + ".mp4";

        if (!destinationFolder.exists())
            destinationFolder.mkdir();

        File subDestinationFolder = new File(uploadPath + File.separator + subFolder);
        if (subDestinationFolder.exists()) {
            for (File file : subDestinationFolder.listFiles()) {
                if (file.isFile())
                    file.delete();
            }
            subDestinationFolder.delete();
        }
        subDestinationFolder.mkdir();

        //获取前端上传的文件列表
        List<MultipartFile> files = ((MultipartHttpServletRequest) request).getFiles("files[]");
        if (files == null || files.size() <= 0)
            files = ((MultipartHttpServletRequest) request).getFiles("files");
        //
        PrintWriter out = null;


        try {
            if (files != null && files.size() > 0)
                out = new PrintWriter(new BufferedWriter(
                        new FileWriter(uploadPath + File.separator + subFolder + File.separator + Constants.tailorIndexFile, true)));
            else {
                if (out != null)
                    out.close();
                ErrorMeasurement em = new ErrorMeasurement();
                em.setTime(System.currentTimeMillis());
                em.setType("save");
                em.setReason("fail to save empty file");
                em.setName("eventvideo");
                Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
                influxDbUtils.influxDB.write(point);
                return new ResponseEntity(new RetMsg(3, "please upload at least 1 file"), HttpStatus.BAD_REQUEST);
            }


            String headFile = "";
            String tailFile = "";

            String indexFile = uploadPath + File.separator + subFolder + File.separator + Constants.tailorIndexFile;
            //if(new File(indexFile).exists()) new File(indexFile).delete();

            String image = uploadPath + File.separator + subFolder + File.separator + Constants.firstFrameImgName;

            final String fileDownloadPath_final = fileDownloadPath.endsWith(File.separator)?fileDownloadPath+cameraId+File.separator:fileDownloadPath+File.separator+cameraId+File.separator;

            String dl_image = fileDownloadPath_final + subFolder + File.separator + Constants.firstFrameImgName;
            String dl_video = fileDownloadPath_final + subFolder + File.separator + destinationFile;
            //if( new File(image).exists())  new File(image).delete();

            String destFile = uploadPath + File.separator + subFolder + File.separator + destinationFile;
            //if(new File(destFile).exists()) new File(destFile).delete();


            int uploadFileCount = 0;
            boolean firstVideoValid = true;

            for (int i = 0; i < files.size(); i++) {

                MultipartFile files1 = files.get(i);
                if (files1.isEmpty()) {
                    log.info("the " + (i + 1) + " file is empty, skipping ... ");
                    if (out != null)
                        out.close();
                    ErrorMeasurement em = new ErrorMeasurement();
                    em.setTime(System.currentTimeMillis());
                    em.setType("save");
                    em.setReason("fail to save empty file");
                    em.setName("eventvideo");
                    Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
                    influxDbUtils.influxDB.write(point);
                    return new ResponseEntity(new RetMsg(5, "The " + (i + 1) + " file is empty, should not upload empty file ..."), HttpStatus.BAD_REQUEST);
                    //return new ResponseEntity<String>("The " + (i+1) + " file is empty, should not upload empty file ...", HttpStatus.BAD_REQUEST);
                }


                String fileName = files1.getOriginalFilename();


                try {
                    final File file = new File(uploadPath + File.separator + subFolder + File.separator + fileName);
                    if (files.size() == 1 && i == 0 && headOffset == 0) {
                        try {
                            files1.transferTo(file);
                        } catch (Exception e) {
                            ErrorMeasurement em = new ErrorMeasurement();
                            em.setTime(System.currentTimeMillis());
                            em.setType("save");
                            em.setReason("fail to save single file");
                            em.setName("eventvideo");
                            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
                            influxDbUtils.influxDB.write(point);
                            return new ResponseEntity(new RetMsg(6, "Save file failed ...  1 of 1"), HttpStatus.BAD_REQUEST);
                        }
                        try {
                            fileTailor.fullVideo(uploadPath + File.separator + subFolder + File.separator + fileName, uploadPath + File.separator + subFolder + File.separator + destinationFile);
                        } catch (Exception e) {
                            ErrorMeasurement em = new ErrorMeasurement();
                            em.setTime(System.currentTimeMillis());
                            em.setType("tailor");
                            em.setReason("fail to tailor the file");
                            em.setName("eventvideo");
                            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
                            influxDbUtils.influxDB.write(point);
                            return new ResponseEntity(new RetMsg(9, "fail to tailor the file 1 of 1"), HttpStatus.BAD_REQUEST);
                        }
                        try {
                            firstFrame.getFirstFrameAsImage(destFile, image, ffmpegpath);
                        } catch (Exception e) {
                            ErrorMeasurement em = new ErrorMeasurement();
                            em.setTime(System.currentTimeMillis());
                            em.setType("getImage");
                            em.setReason("fail to get the first frame as image");
                            em.setName("eventvideo");
                            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
                            influxDbUtils.influxDB.write(point);
                            return new ResponseEntity(new RetMsg(9, "fail to get the first frame as image"), HttpStatus.BAD_REQUEST);
                        }
                        log.info("The " + (i + 1) + " file is uploaded, total " + files.size() + " file(s) and the headOffset is 0 " + headOffset);
                        EventMeasurement em = new EventMeasurement();
                        em.setTime(String.valueOf(System.currentTimeMillis()));
                        em.setType("upload");
                        em.setName("eventvideo");
                        em.setEvent(1);
                        em.setFiles(i);
                        Point point = Point.measurementByPOJO(EventMeasurement.class).addFieldsFromPOJO(em).build();
                        return new ResponseEntity(new RetMsg(0, "OK"), HttpStatus.OK);
                    } else if (files.size() == 1 && i == 0 && headOffset > 0) {
                        files1.transferTo(file);
                        fileTailor.tailorSingleVideo(uploadPath + File.separator + subFolder + File.separator + fileName, destFile, image, headOffset, ffmpegpath,startTime,endTime,cameraId,dl_video,dl_image);
                        log.info("The " + (i + 1) + " file is uploaded, total " + files.size() + " file(s) and the headOffset is >>> " + headOffset);

                        EventMeasurement em = new EventMeasurement();
                        em.setTime(String.valueOf(System.currentTimeMillis()));
                        em.setType("upload");
                        em.setName("eventvideo");
                        em.setEvent(1);
                        em.setFiles(i);
                        Point point = Point.measurementByPOJO(EventMeasurement.class).addFieldsFromPOJO(em).build();
                        influxDbUtils.influxDB.write(point);

                        return new ResponseEntity(new RetMsg(0, "OK"), HttpStatus.OK);
                    } else {
                        try {
                            files1.transferTo(new File(uploadPath + File.separator + subFolder + File.separator + i + ".mp4"));
                        } catch (Exception e) {
                            ErrorMeasurement em = new ErrorMeasurement();
                            em.setTime(System.currentTimeMillis());
                            em.setType("save");
                            em.setReason("fail to save multiple files");
                            em.setName("eventvideo");
                            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
                            influxDbUtils.influxDB.write(point);
                            return new ResponseEntity(new RetMsg(6, "Save file failed  \" + i + \" of \"+ files.size()+"), HttpStatus.BAD_REQUEST);
                        }
                        log.info("The " + (i + 1) + " file is uploaded, total " + files.size() + " file(s) and the headOffset is >>> " + headOffset + " and the tail offset is " + tailOffset);
                        if(i==0){
                            firstVideoValid = firstFrame.validVideo(uploadPath + File.separator + subFolder + File.separator + "0.mp4");
                        }

                        if (i == 0 && firstVideoValid)
                            headFile = uploadPath + File.separator + subFolder + File.separator + "0.mp4";
//                        if(i == 1 && !firstVideoValid)
//                            headFile = uploadPath + File.separator + subFolder + File.separator + i + "1.mp4";
                        if (i == files.size() - 1)
                            tailFile = uploadPath + File.separator + subFolder + File.separator + i + ".mp4";


                        //prepare the concate index file
                        if (firstVideoValid) {
                            if ((i == 0 && headOffset == 0) || (i == files.size() - 1 && tailOffset == 0))
                                out.println("file " + i + ".mp4");
                            else if ((i == 0 && headOffset > 0) || (i == files.size() - 1 && tailOffset > 0))
                                out.println("file " + i + Constants.tailorVideoPrefix + ".mp4");
                            else
                                out.println("file " + i + ".mp4");
                        } else {
                            if(i!=0)
                            out.println("file " + i + ".mp4");
                        }


                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    log.info("The " + (i++) + " file is not uploaded");
                    ErrorMeasurement em = new ErrorMeasurement();
                    em.setTime(System.currentTimeMillis());
                    em.setType("save");
                    em.setReason("fail to save multiple files");
                    em.setName("eventvideo");
                    Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
                    influxDbUtils.influxDB.write(point);
                    return new ResponseEntity(new RetMsg(4, "The " + (i++) + " file is not uploaded"), HttpStatus.BAD_REQUEST);

                    //return new ResponseEntity<String>("The " + (i++) + " file is not uploaded", HttpStatus.BAD_REQUEST);
                }


            }

            if (out != null)
                out.close();

            try {
                fileTailor.tailorMultiple(headFile, tailFile, indexFile, destFile, image, headOffset, tailOffset, ffmpegpath,startTime,endTime,cameraId,dl_video,dl_image);
            } catch (Exception e) {
                e.printStackTrace();
                ErrorMeasurement em = new ErrorMeasurement();
                em.setTime(System.currentTimeMillis());
                em.setType("tailor");
                em.setReason("fail to merge files");
                em.setName("eventvideo");
                Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
                influxDbUtils.influxDB.write(point);
                return new ResponseEntity(new RetMsg(8, "fail to merge"), HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e) {
            e.printStackTrace();
            log.info("Error occured while uploading file ...");
            ErrorMeasurement em = new ErrorMeasurement();
            em.setTime(System.currentTimeMillis());
            em.setType("save");
            em.setReason("fail to save multiple files");
            em.setName("eventvideo");
            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
            influxDbUtils.influxDB.write(point);
            return new ResponseEntity(new RetMsg(7, "FAIL TO UPLOAD FILES ..."), HttpStatus.BAD_REQUEST);
            //return new ResponseEntity<String>("FAIL TO UPLOAD FILES ...", HttpStatus.BAD_REQUEST);
        } finally {
            if (out != null)
                out.close();
        }
        EventMeasurement em = new EventMeasurement();
        em.setTime(String.valueOf(System.currentTimeMillis()));
        em.setType("upload");
        em.setName("eventvideo");
        em.setEvent(1);
        em.setFiles(files.size());
        Point point = Point.measurementByPOJO(EventMeasurement.class).addFieldsFromPOJO(em).build();
        influxDbUtils.influxDB.write(point);
        return new ResponseEntity(new RetMsg(0, "OK"), HttpStatus.OK);
    }

}
