package com.ilabservice.hlslive;


import com.ilabservice.hlslive.ffmpeg.FileTailor;
import com.ilabservice.hlslive.ffmpeg.FirstFrame;
import com.ilabservice.hlslive.utils.Handler;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFprobe;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.servlet.MultipartConfigElement;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@Configuration
@ComponentScan
@EnableScheduling
@EnableSwagger2
public class HlsliveApplication {

    public static void main(String[] args) {
        SpringApplication.run(HlsliveApplication.class, args);

    }

    @Bean
    public Map<String,Process> processMap(){return new HashMap<String, Process>();}


    @Value("${ffmpegpath}")
    private String ffmpegpath;

    @Value("${ffprobepath}")
    private String ffprobepath;

    @Bean
    public FFmpeg ffmpeg() {

        FFmpeg ffmpeg = null;
        try {
            ffmpeg = new FFmpeg(ffmpegpath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ffmpeg;
    }

    @Bean
    FFprobe ffprobe() {
        FFprobe ffprobe = null;

        try {
            ffprobe = new FFprobe(ffprobepath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ffprobe;
    }

    @Bean
    FirstFrame firstFrame(){
        return new FirstFrame();
    }

    @Bean
    public Handler handler(){return  new Handler();}
    @Bean
    FileTailor fileTailor(){
        return new FileTailor();
    }

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        //允许上传的文件最大值
        factory.setMaxFileSize("9000MB"); //KB,MB
        /// 设置总上传数据总大小
        factory.setMaxRequestSize("1000MB");
        return factory.createMultipartConfig();
    }

}
