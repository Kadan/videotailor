package com.ilabservice.hlslive.repository;

import com.ilabservice.hlslive.entity.EvtVideo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VideoRepository extends JpaRepository<EvtVideo,Long> {
    List<EvtVideo> findByVideo(String video);
}
