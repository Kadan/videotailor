package com.ilabservice.hlslive.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.integration.mqtt.support.MqttHeaders;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.messaging.handler.annotation.Header;

@Configuration
public class MQTTConfiguration {
    @Value("${mqtt.producer.servers}")
    private String servers;
    @Value("${mqtt.producer.client-id}")
    private String clientId;
    @Value("${mqtt.producer.default-topic}")
    private String defaultTopic;
    @Value("${mqtt.producer.default-qos}")
    private int qos;
    @Value("{mqtt.connect.username}")
    private String username;
    @Value("{mqtt.connect.password}")
    private String password;

    @Bean
    public MqttPahoClientFactory mqttClientFactory() {
        DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
        factory.setConnectionOptions(getSenderMqttConnectOptions());
        return factory;
    }


    @Bean
    public MqttConnectOptions getSenderMqttConnectOptions(){
        MqttConnectOptions options=new MqttConnectOptions();
        if(!username.trim().equals("")){
            options.setUserName(username);
        }
        options.setPassword(password.toCharArray());
        options.setServerURIs(StringUtils.split(servers, ","));
        options.setConnectionTimeout(10);
        options.setKeepAliveInterval(20);
        options.setCleanSession(false);
//        options.setWill("willTopic", WILL_DATA, 2, false);
        return options;
    }


    @Bean
    @ServiceActivator(inputChannel = "mqttOutboundChannel")
    public MessageHandler mqttOutbound() {
        MqttPahoMessageHandler messageHandler = new MqttPahoMessageHandler(clientId, mqttClientFactory());
        messageHandler.setAsync(true);
        messageHandler.setDefaultQos(qos);
        messageHandler.setDefaultTopic(defaultTopic);
        return messageHandler;
    }

    @Bean
    public MessageChannel mqttOutboundChannel() {
        return new DirectChannel();
    }

    @MessagingGateway(defaultRequestChannel = "mqttOutboundChannel")
    public interface MqttGateway {
        void sendToMqtt(String payload);
        void sendToMqtt(@Header(MqttHeaders.TOPIC) String topic, String payload);
        void sendToMqtt(@Header(MqttHeaders.TOPIC) String topic, @Header(MqttHeaders.QOS) int qos, String payload);
    }

//    @Bean
//    public MessageChannel mqttInputChannel() {
//        return new DirectChannel();
//    }
//
//    @Bean
//    public MessageProducer inbound() {
//        MqttPahoMessageDrivenChannelAdapter adapter = new MqttPahoMessageDrivenChannelAdapter("consumerid",
//                mqttClientFactory(), "video.ai/v1.0/task");
//        adapter.setCompletionTimeout(5000);
//        adapter.setConverter(new DefaultPahoMessageConverter());
//        adapter.setQos(1);
//        adapter.setOutputChannel(mqttInputChannel());
//        return adapter;
//    }
//
//    @Bean
//    @ServiceActivator(inputChannel = "mqttInputChannel")
//    public MessageHandler messageHandler() {
//        return message -> {
//            String payload = message.getPayload().toString();
//            String topic = message.getHeaders().get("mqtt_receivedTopic").toString();
//            if (topic.equals("video.ai/v1.0/task")) {
//                System.out.println("ILAB_VIDEO_SVC: handling message " + payload);
//            } else if (topic.equals("topic2")) {
//                System.out.println("topic2: handling message " + payload);
//            } else {
//                System.out.println(topic + ": abandon ... " + payload);
//            }
//        };
//    }

}
