package com.ilabservice.hlslive.entity;


import javax.persistence.*;

@Entity
public class EvtVideo {
    @Column(nullable = false)
    public String video;
    @Column(nullable = true)
    public String image;
    @Column(nullable = true)
    public Long startTime;
    @Column(nullable = true)
    public Long endTime;
    @Column(nullable = true)
    public Long length;

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    @Column(nullable = true)
    public Long size;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(nullable = true)
    public String status;


    public String getVaresult() {
        return varesult;
    }

    public void setVaresult(String varesult) {
        this.varesult = varesult;
    }

    @Column(columnDefinition = "text",nullable = true)
    @Lob
    @Basic(fetch=FetchType.LAZY)
    public String varesult;

    public Long getVaStartTime() {
        return vaStartTime;
    }

    public void setVaStartTime(Long vaStartTime) {
        this.vaStartTime = vaStartTime;
    }

    public Long getVaEndTime() {
        return vaEndTime;
    }

    public void setVaEndTime(Long vaEndTime) {
        this.vaEndTime = vaEndTime;
    }

    @Column(nullable = true)
    public Long vaStartTime;
    @Column(nullable = true)
    public Long vaEndTime;

    public String getVaCode() {
        return vaCode;
    }

    public void setVaCode(String vaCode) {
        this.vaCode = vaCode;
    }

    @Column(nullable = true)
    public String vaCode;


    public String getCameraId() {
        return cameraId;
    }

    public void setCameraId(String cameraId) {
        this.cameraId = cameraId;
    }

    @Column(nullable = true)
    public String cameraId;
    @Id
    @GeneratedValue
    private Long id;


    public EvtVideo(String video, Long startTime, Long endTime, String image, Long length){
        this.video = video;
        this.startTime = startTime;
        this.endTime = endTime;
        this.image = image;
        this.length = length;

    }
    //default constructor
    public EvtVideo(){}

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }



    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }


}