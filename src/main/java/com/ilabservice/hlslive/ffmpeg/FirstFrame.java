package com.ilabservice.hlslive.ffmpeg;

import com.ilabservice.hlslive.utils.Handler;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import net.bramp.ffmpeg.probe.FFmpegFormat;
import net.bramp.ffmpeg.probe.FFmpegProbeResult;
import net.bramp.ffmpeg.probe.FFmpegStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.Fraction;
import org.apache.tomcat.jni.Local;
import org.influxdb.dto.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FirstFrame {

    private final static Logger log = LoggerFactory.getLogger(FirstFrame.class);

//    public static void main(String[] args){
//
//
////        String dateTime;
////        long timeLong = Long.valueOf(1513937933);
////        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
////        dateTime = simpleDateFormat.format(new Date(timeLong * 1000L+999));
//
//
//
////        System.out.println(dateTime);
//        String video = "/Users/lijunjie/downloads/20191129021143000-20191129025101999.mp4";
//        String image = "/Users/lijunjie/KadanLAB/data/222.jpg";
////new FirstFrame().validVideo(15);
//        //new FirstFrame().getFirstFrameAsImage(video,image,"/usr/local/bin/ffmpeg");
////        new FirstFrame().getVideoTime("/Users/lijunjie/KadanLAB/data/C90840818/1563937933-1563938933/1563937933-1563938933.mp4","/usr/local/bin/ffmpeg");
//    }

    @Autowired
    Handler handler;

    @Autowired
    FFprobe ffprobe;

    @Autowired
    FFmpeg ffmpeg;

    public void getFirstFrameAsImage(String inputVideo, String image, String ffmpegpath) {

//        FFmpegBuilder builder =
//                new FFmpegBuilder()
//                        .addExtraArgs("-ss")
//                        .addExtraArgs("00:00:01")
//                        .addExtraArgs("-t")
//                        .addExtraArgs("00:00:02")
//                        .addInput(inputVideo)
//                        .addOutput(image)
//                        .addExtraArgs("-f")
//                        .addExtraArgs("image2")
//                        .done();
//        FFmpegExecutor executor = null;
//        try {
//            executor = new FFmpegExecutor(new FFmpeg(ffmpegpath));
//            executor.createJob(builder).run();
//        } catch (Exception e) {
//            //e.printStackTrace();
//        }


        List<String> commands = new java.util.ArrayList<String>();
        commands.add(ffmpegpath);
        commands.add("-i");
        commands.add(inputVideo);
        commands.add("-ss");
        commands.add("00:00:02");
        commands.add("-t");
        commands.add("00:00:03");
        commands.add("-f");
        commands.add("image2");
        commands.add(image);
        ProcessBuilder builder = new ProcessBuilder();
        builder.command(commands);
        try {
            final Process p = builder.start();
            Thread.sleep(1000);
            stopImageCapture(image, inputVideo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopImageCapture(String image, String inputVideo) {
        if (new File(image).exists()) {
            log.info("the image exists, stop the process if any resist >>> " + image);
            try {
                handler.stopImageCapture(image, inputVideo);
            } catch (Exception e) {
                log.info("can not stop the process of image capture >>> " + inputVideo);
            }
        } else {
            try {
                Thread.sleep(1000);
                if (new File(image).exists()) {
                    log.info("the image exists, stop the process if any resist >>> " + image);
                    try {
                        handler.stopImageCapture(image, inputVideo);
                    } catch (Exception e) {
                        log.info("can not stop the process of image capture >>> " + inputVideo);
                    }
                } else
                    handler.stopImageCapture(image, inputVideo);

            } catch (InterruptedException e) {
            }

        }
    }


    public void concat(String input, String output) {
        FFmpegBuilder builder =
                new FFmpegBuilder()
                        .setFormat("concat")
                        .addInput(input)
                        .addOutput(output)
                        .setVideoCodec("copy")
                        .setAudioCodec("copy")
                        .setFormat("mp4")
                        .done();
        FFmpegExecutor executor = null;
        try {
            executor = new FFmpegExecutor(ffmpeg, ffprobe);
        } catch (Exception e) {
            e.printStackTrace();
        }
        executor.createJob(builder).run();
    }


//    public void concat(String input, String output){
//        FFmpegBuilder builder =
//                new FFmpegBuilder()
//                        .setFormat("concat")
//                        .addInput(input)
//                        .addOutput(output)
//                        .setVideoCodec("copy")
//                        .setAudioCodec("copy")
//                        .done();
//        FFmpegExecutor executor = null;
//        try {
//            executor = new FFmpegExecutor(new FFmpeg("/usr/local/bin/ffmpeg"),new FFprobe("/usr/local/bin/ffprobe"));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        executor.createJob(builder).run();
//    }

//    public static void main(String[] args) {
//        try {
//            FFmpegProbeResult probeResult = new FFprobe("/usr/local/bin/ffprobe").probe("/Users/lijunjie/Downloads/1.mp4");
//
//            FFmpegStream v = probeResult.streams.get(0);
//            Fraction r = v.avg_frame_rate;
//            Long f = v.bit_rate;
//            System.out.println(">>>" + probeResult.format.bit_rate/1000L);
//            System.out.println(">>>" + r.intValue());
////            int rrr = r.intValue();
////            if (r.intValue() > 10)
////                return true;
////            else
////                return false;
//
//        } catch (IOException e) {
//            e.printStackTrace();
////            return false;
//        }
//    }

    public boolean validVideo(String video) {
        try {
            log.info("validating the first video " + video);
            FFmpegProbeResult probeResult = ffprobe.probe(video);
            FFmpegStream v = probeResult.streams.get(0);
            Long bitrate =  probeResult.format.bit_rate;
            Fraction r = v.avg_frame_rate;
            log.info("validating the first video >>> bitrate is " + bitrate/1000L);
            log.info("validating the first video >>> frame rate is " + r.intValue());
//            int rrr = r.intValue();
            if (r.intValue() > 10 && bitrate/1000L>50)
                return true;
            else
                return false;

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public int getVideoTime(String video_path) {

        try {
            FFmpegProbeResult probeResult = ffprobe.probe(video_path);
            FFmpegFormat format = probeResult.getFormat();
            double d = format.duration;
            int duration = Double.valueOf(d).intValue();
            String dd = String.valueOf(d);
            if (Integer.valueOf(StringUtils.substringAfter(dd, ".")) > 0)
                return (duration + 1);
            else
                return duration;

        } catch (IOException e) {
            e.printStackTrace();
//            ErrorMeasurement em = new ErrorMeasurement();
//            em.setTime(System.currentTimeMillis());
//            em.setType("videoRecorder");
//            em.setReason("failed to get video length");
//            em.setName("videoRecorder");
//            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
//            influxDbUtils.influxDB.write(point);
            return 0;
        }


//    public  int getVideoTime(String video_path, String ffmpeg_path) {
//        List<String> commands = new java.util.ArrayList<String>();
//        commands.add(ffmpeg_path);
//        commands.add("-i");
//        commands.add(video_path);
//        try {
//            ProcessBuilder builder = new ProcessBuilder();
//            builder.command(commands);
//            final Process p = builder.start();
//
//            //从输入流中读取视频信息
//            BufferedReader br = new BufferedReader(new InputStreamReader(p.getErrorStream()));
//            StringBuffer sb = new StringBuffer();
//            String line = "";
//            while ((line = br.readLine()) != null) {
//                sb.append(line);
//            }
//            br.close();
//
//            //从视频信息中解析时长
//            String regexDuration = "Duration: (.*?), start: (.*?), bitrate: (\\d*) kb\\/s";
//            Pattern pattern = Pattern.compile(regexDuration);
//            Matcher m = pattern.matcher(sb.toString());
//            if (m.find()) {
//                int time = getTimelen(m.group(1));
//                log.info(video_path+ ", length："+time+", start@ ："+m.group(2)+", Bits："+m.group(3)+"kb/s");
//                return time;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return 0;
//    }

    }

        private int getTimelen(String timelen){
        int min = 0;
        String strs[] = timelen.split(":");
        if (strs[0].compareTo("0") > 0) {
            min += Integer.valueOf(strs[0]) * 60 * 60;//秒
        }
        if (strs[1].compareTo("0") > 0) {
            min += Integer.valueOf(strs[1]) * 60;
        }
        if (strs[2].compareTo("0") > 0) {
            min += Math.round(Float.valueOf(strs[2]));
        }
        return min;
    }

}