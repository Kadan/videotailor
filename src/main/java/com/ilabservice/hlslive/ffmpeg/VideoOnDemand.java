package com.ilabservice.hlslive.ffmpeg;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

//@RestController

@Deprecated
public class VideoOnDemand {

    @Autowired
    Map<String, Process> processMap;

    @RequestMapping(value = "/start",method = RequestMethod.GET)
    public void start(){
        List<String> commands = new java.util.ArrayList<String>();
        commands.add("/usr/local/bin/ffmpeg");
        commands.add("-i");
        commands.add("rtmp://live.chosun.gscdn.com/live/tvchosun1.stream");
        commands.add("-f");
        commands.add("mp4");
        commands.add("-vcodec");
        commands.add("copy");
        commands.add("-acodec");
        commands.add("copy");
        commands.add("-bsf:a");
        commands.add("aac_adtstoasc");
        commands.add("/Users/lijunjie/KadanLAB/data/recorde.mp4");
        ProcessBuilder builder = new ProcessBuilder();
        builder.command(commands);
        try { final Process p = builder.start();
            processMap.put("ttt",p);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/stop",method = RequestMethod.GET)
    public void stop(){
        processMap.get("ttt").destroy();
    }

}
