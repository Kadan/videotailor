package com.ilabservice.hlslive.ffmpeg;

import com.alibaba.fastjson.JSON;
import com.ilabservice.hlslive.config.MQTTConfiguration;
import com.ilabservice.hlslive.constants.Constants;
import com.ilabservice.hlslive.entity.EvtVideo;
import com.ilabservice.hlslive.entity.MQTTEnvelope;
import com.ilabservice.hlslive.repository.VideoRepository;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class FileTailor {


    @Autowired
    FFmpeg fFmpeg;

    @Autowired
    FFprobe fFprobe;

    @Autowired
    FirstFrame firstFrame;

    @Autowired
    VideoRepository videoRepository;

    @Autowired
    private MQTTConfiguration.MqttGateway mqttGateway;

    @Value("${mqtt.producer.default-topic}")
    private String defaultTopic;

    private final static Logger logger = LoggerFactory.getLogger(FileTailor .class);


    private static ThreadFactory videotailorFactory = new ThreadFactory() {
        private final AtomicInteger counter = new AtomicInteger();
        @Override
        public Thread newThread(Runnable runable) {
            return new Thread(runable, "### tailor and merge video thread ### " + counter.getAndIncrement());
        }
    };

    /**prepare the threadpool*/
    private static ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(100, 300, 3, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(100),videotailorFactory);


    public void tailor(String folder, String startTime, String endTime, int startDuration, int endDuration,String ffmpegpath,String cameraId){
        threadPoolExecutor.submit(
                new Callable<Integer>() {
                    @Override
                    public Integer call() throws Exception {
                        taiorAndMerge(folder, startTime, endTime, startDuration, endDuration, ffmpegpath);
                        return 0;
                    }
                });
    }

    public void tailorSingle(String input, String output,String image, int startOffSet, String ffmpegpath,int startTime,int endTime,String cameraId,
                             String dl_video,String dl_image){
        threadPoolExecutor.submit(
                new Callable<Integer>() {
                    @Override
                    public Integer call() throws Exception {
                        tailorSingleVideo(input,output,image,startOffSet,ffmpegpath,startTime,endTime,cameraId,dl_video,dl_image);
                        return 0;
                    }
                });
    }


    public void tailorSingleVideo(String input, String output,String image, int startOffSet,
                                  String ffmpegpath,int startTime,int endTime,String cameraId,
                                  String dl_video,String dl_image){
        tailorVideo(input,output,startOffSet,fFmpeg,fFprobe);
        firstFrame.getFirstFrameAsImage(output,image,ffmpegpath);
        EvtVideo video = new EvtVideo();
        video.setEndTime((long)endTime*1000L+999L);
        video.setStartTime((long)startTime*1000L);
        video.setVideo(dl_video);
        video.setImage(dl_image);
        video.setCameraId(cameraId);
        video.setSize(new File(output).length());
        video.setStatus(Constants.CREATED);
        video.setLength((long)firstFrame.getVideoTime(output));
        video.setVaStartTime(System.currentTimeMillis());
        videoRepository.save(video);
        MQTTEnvelope mqttEnvelope = new MQTTEnvelope();
//        mqttEnvelope.setType(Constants.VIDEO_TYPE_EVT);
        mqttEnvelope.setVideo(video);
        logger.info("sending message to mqtt " + JSON.toJSONString(mqttEnvelope));
        mqttGateway.sendToMqtt(defaultTopic, JSON.toJSONString(mqttEnvelope));
    }



    public void tailorMultiple(String headFile, String tailFile, String indexFile, String outputFile, String image,
                               int startDuration, int endDuration, String ffmpegpath,int startTime,int endTime,String cameraId,
                               String dl_video,String dl_image){
        threadPoolExecutor.submit(
                new Callable<Integer>() {
                    @Override
                    public Integer call() throws Exception {
                        tailorAndMerge( headFile,  tailFile,  indexFile,  outputFile,  image,  startDuration,  endDuration,  ffmpegpath);
                        EvtVideo video = new EvtVideo();
                        video.setEndTime((long)endTime*1000L+999L);
                        video.setStartTime((long)startTime*1000L);
                        video.setVideo(dl_video);
                        video.setImage(dl_image);
                        video.setCameraId(cameraId);
                        video.setSize(new File(outputFile).length());
                        video.setStatus(Constants.CREATED);
                        video.setLength((long)firstFrame.getVideoTime(outputFile));
                        video.setVaStartTime(System.currentTimeMillis());
                        videoRepository.save(video);
                        MQTTEnvelope mqttEnvelope = new MQTTEnvelope();
//                        mqttEnvelope.setType(Constants.VIDEO_TYPE_EVT);
                        mqttEnvelope.setVideo(video);
                        logger.info("sending message to mqtt " + JSON.toJSONString(mqttEnvelope));
                        mqttGateway.sendToMqtt(defaultTopic, JSON.toJSONString(mqttEnvelope));
                        return 0;
                    }
                });
    }



    private void tailorAndMerge(String headFile, String tailFile, String indexFile, String outputFile, String image, int startDuration, int endDuration, String ffmpegpath){

        if(startDuration > 0)
            tailorVideo(headFile,StringUtils.substringBefore(headFile,".mp4")+Constants.tailorVideoPrefix + ".mp4",startDuration,fFmpeg,fFprobe);
        if(endDuration > 0)
            tailorVideo(tailFile,StringUtils.substringBefore(tailFile,".mp4")+Constants.tailorVideoPrefix + ".mp4",endDuration,fFmpeg,fFprobe);

        firstFrame.concat(indexFile,outputFile);

        firstFrame.getFirstFrameAsImage(outputFile,image,ffmpegpath);

    }


    private void taiorAndMerge(String folder, String startTime, String endTime, int startDuration, int endDuration,String ffmpegpath) {
        PrintWriter out = null;
        String destinationFile = startTime + "-" + endTime + ".mp4";
        new File(folder + File.separator + destinationFile).deleteOnExit();
        new File(folder + File.separator + Constants.tailorIndexFile).deleteOnExit();
        new File(folder + File.separator + Constants.firstFrameImgName).deleteOnExit();

        try {
            File file = new File(folder);
             out = new PrintWriter(new BufferedWriter(
                    new FileWriter(folder + "/" + Constants.tailorIndexFile, true)));
            List<File> files = Arrays.stream(file.listFiles()).filter(f -> f.isFile() && f.getName().contains("mp4")).collect(Collectors.toList());
            try{
            files.sort(new MyComparator());}catch(Exception e){
                logger.info("error occured while sorting the uploaded files... use the default sequence");

            }

            if (files.size() <= 0)
                return;
            //new FirstFrame().getFirstFrameAsImage(folder + "/" + files.get(0).getName(), folder + "/" + Constants.firstFrameImgName,ffmpegpath);

            if (files.size() == 1) {
                logger.info("Only 1 file received ...");
                if(startDuration >0)
                    tailorVideo(folder + "/" + files.get(0).getName(), folder + File.separator + destinationFile, startDuration,fFmpeg,fFprobe);
                files.get(0).renameTo(new File(folder + File.separator + destinationFile));
            }else{
                tailorVideo(folder + "/" + files.get(0).getName(), folder + File.separator + Constants.tailorVideoPrefix + files.get(0).getName(), startDuration,fFmpeg,fFprobe);
                files.get(0).renameTo(new File(StringUtils.substringBefore(folder + File.separator + files.get(0).getName(),".mp4")));

                tailorVideo(folder + "/" + files.get(files.size()-1).getName(), folder + File.separator + Constants.tailorVideoPrefix + files.get(files.size()-1).getName(), endDuration,fFmpeg,fFprobe);
                files.get(files.size()-1).renameTo(new File(StringUtils.substringBefore(folder + File.separator + files.get(files.size()-1).getName(),".mp4")));

                for(int i=0;i<files.size();i++){
                    if(i==0 || i==files.size()-1)
                        out.println("file " + Constants.tailorVideoPrefix+ files.get(i).getName());
                    else
                        out.println("file " + files.get(i).getName());
                }

                logger.info("get the first frame as the image ...");
                firstFrame.getFirstFrameAsImage(folder + "/" + Constants.tailorVideoPrefix + files.get(0).getName(), folder + "/" + Constants.firstFrameImgName,ffmpegpath);

            }
            if(out !=null )
                out.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(out !=null )
                out.close();
        }
        new File(folder + "/"+destinationFile).deleteOnExit();
        firstFrame.concat(folder + "/" + Constants.tailorIndexFile,folder + "/"+destinationFile);

    }


    public void fullVideo(String input, String output) {
        logger.info("just transfer with ffmpeg ...");

        FFmpegBuilder builder =
                new FFmpegBuilder()
                        .addInput(input)
                        .addOutput(output)
                        //.setDuration(startOffSet, TimeUnit.SECONDS)
                        .setVideoCodec("copy")
                        .setAudioCodec("copy")
                        .done();
        FFmpegExecutor executor = null;
        try {
            executor = new FFmpegExecutor(fFmpeg, fFprobe);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("file tailor failed");
        }
        executor.createJob(builder).run();

    }

//    private static final Logger log = LoggerFactory.getLogger(FileTailor.class);

    private void tailorVideo(String input, String output,int startOffSet,FFmpeg fFmpeg, FFprobe fFprobe) {
        logger.info("tailoring file with offset " + startOffSet );

        FFmpegBuilder builder =
                new FFmpegBuilder()
                        .addInput(input)
                        .addOutput(output)
                        .setDuration(startOffSet, TimeUnit.SECONDS)
                        .setVideoCodec("copy")
                        .setAudioCodec("copy")
                        .done();
        FFmpegExecutor executor = null;
        try {
            executor = new FFmpegExecutor(fFmpeg, fFprobe);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("file tailor failed");
        }
        executor.createJob(builder).run();

    }

    private class MyComparator implements Comparator<Object>
    {
        @Override
        public int compare(Object o1, Object o2) {
            File s1 = (File) o1;
            File s2 = (File) o2;
            double start = Double.valueOf(StringUtils.substringBefore(StringUtils.substringAfterLast(s1.getName(),"-"),".mp4").replaceAll("-","").replaceAll("_",""));
            double end = Double.valueOf(StringUtils.substringBefore(StringUtils.substringAfterLast(s2.getName(),"-"),".mp4").replaceAll("-","").replaceAll("_",""));
            if(start>end)
                return 1;
            if(start<end)
                return -1;
            else
                return 0;
        }
    }

}
