package com.ilabservice.hlslive.measurements;

import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;

@Measurement(name="EVENT_MEASUREMENTS")
public class EventMeasurement {

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="time")
    private String time;
    @Column(name="type")
    private String type;
    @Column(name="name")
    private String name;

    public int getEvent() {
        return event;
    }

    public void setEvent(int event) {
        this.event = event;
    }

    @Column(name="event")
    private int event;

    public int getFiles() {
        return files;
    }

    public void setFiles(int files) {
        this.files = files;
    }

    @Column(name="files")
    private int files;
}
