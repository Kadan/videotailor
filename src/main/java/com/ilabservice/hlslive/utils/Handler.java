package com.ilabservice.hlslive.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Handler {
    @Value("${ffmpegpath}")
    private String ffmpeg;

    private static final Logger log = LoggerFactory.getLogger(Handler.class);

    public void stopImageCapture(String image, String video) {
        List<String> PID = this.getPID(ffmpeg,image,video,"-f image2");
        PID.stream().forEach(s->closeLinuxProcess(s));
    }




    /**
     * 获取Linux进程的PID
     * @param command
     * @return
     */
    public List<String> getPID(String command, String key, String key1, String key2){
        BufferedReader reader =null;
        List<String> PIDs = new ArrayList<String>();
        try{
            //显示所有进程
            Process process = Runtime.getRuntime().exec("ps -ef");
            reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = null;
            while((line = reader.readLine())!=null){
                if(line.contains(command) && line.contains(key) && line.contains(key1)&& line.contains(key2)){
//                   log.info("found processs "+line);
                    String[] strs = line.split("\\s+");
//                    log.info("strs[0] "+strs[0]);
//                    log.info("strs[1] "+strs[1]);
//                    log.info("strs[2] "+strs[2]);
                    PIDs.add(strs[1]);
                }
            }
            return PIDs;
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            if(reader!=null){
                try {
                    reader.close();
                } catch (IOException e) {

                }
            }
        }
        return PIDs;
    }

    /**
     * 关闭Linux进程
     * @param Pid 进程的PID
     */
    public void closeLinuxProcess(String Pid){
        Process process = null;
        BufferedReader reader =null;
        try{
            //kill the process
            process = Runtime.getRuntime().exec("kill -15 "+Pid);
            reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = null;
            while((line = reader.readLine())!=null){
                log.info("kill PID return info -----> "+line);
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            if(process!=null){
                process.destroy();
            }

            if(reader!=null){
                try {
                    reader.close();
                } catch (IOException e) {

                }
            }
        }
    }
}