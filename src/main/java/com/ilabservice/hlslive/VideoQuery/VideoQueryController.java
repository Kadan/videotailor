package com.ilabservice.hlslive.VideoQuery;

import com.ilabservice.hlslive.constants.Constants;
import com.ilabservice.hlslive.ffmpeg.FirstFrame;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping(value="/query")
@Api(value="event viode query")
public class VideoQueryController {
    @Value("${fileUploadPath}")
    private String fileUploadPath;

    @Value("${fileDownloadPath}")
    private String fileDownloadPath;

    @Autowired
    FirstFrame firstFrame;

    @Value("${ffmpegpath}")
    private String ffmpegpath;

    @GetMapping("/evtvideoStatus/{cameraId}")
    @ApiOperation(value=" query the video merge status")
    public String getVideoTailorStatud(HttpServletRequest request, @RequestParam(value = "startTime",required = true) int startTime,
                                       @RequestParam(value="endTime",required = true) int endTime,
                                       @PathVariable String cameraId){
        if(StringUtils.isEmpty(cameraId))
            return "Please provide the dcameraId";

        String destFolder = fileUploadPath + cameraId + File.separator + startTime + "-" + endTime;

        File destFolders = new File(destFolder);

        if(destFolders!=null && !destFolders.exists())
            return "No such directory match the query conditions[cameraid]/[startTime]-[endTime] .. cameraId >>> " + cameraId + " startTime >>> " + startTime + " endTime >>> " + endTime;

        File destFile = new File(destFolder + File.pathSeparator + startTime+"-"+endTime + ".mp4");

        if(destFolders!=null &&!destFolders.exists())
            return "merged error or merging in progress ... ";
        if(destFile.exists() && destFile.canRead())
            return  "READY";

        return "READY";
    }

//
//    @GetMapping("/evtvideos/{cameraId}")
//    //@PostMapping("/evtvideos/{cameraId}")
//    @ApiOperation(value="get the event videos by cameraid, starttime and endtime")
//    public ResponseEntity getVideosByCameraID(HttpServletRequest request, @RequestParam(value = "startTime",required = true) int startTime,
//                                                      @RequestParam(value="endTime",required = true) int endTime,
//                                                      @PathVariable String cameraId){
//
//        List<Video> videos = new ArrayList<Video>();
//
//        if(startTime >= endTime)
//            return new ResponseEntity<String>("query time range is not correct - startTime is ahead of endtime " + startTime + " - endTime " + startTime, HttpStatus.BAD_REQUEST);
//
//        if(StringUtils.isEmpty(cameraId))
//            return new ResponseEntity<String>("Please provide the cameraId " + cameraId, HttpStatus.BAD_REQUEST);
//        String destFolder = fileUploadPath + cameraId;
//        String downloadFolder = fileDownloadPath + cameraId;
//
//        File destFolders = new File(destFolder);
//        if(destFolders!=null && !destFolders.exists())
//            return new ResponseEntity<String>("The camera ID " + cameraId + " does not exists or the query time range is wrong startTime" +  startTime + " endTime " + endTime, HttpStatus.BAD_REQUEST);
//            //return "No such directory match the query conditions[cameraid]/[startTime]-[endTime] .. cameraId >>> " + cameraId + " startTime >>> " + startTime + " endTime >>> " + endTime;
//
//
//        List<String> subFolders = Stream.of(destFolders.listFiles()).filter(s->s.isDirectory())
//                .map(s->s.getName())
//                .filter(s-> (startTime<=Integer.valueOf(StringUtils.substringBefore(s,"-")).intValue() &&
//                                endTime > Integer.valueOf(StringUtils.substringBefore(s,"-")).intValue() &&
//                                    endTime <=Integer.valueOf(StringUtils.substringAfter(s,"-")).intValue()
//                        ) ||
//                        (Integer.valueOf(StringUtils.substringBefore(s,"-")).intValue() >= startTime &&
//                                Integer.valueOf(StringUtils.substringAfter(s,"-")).intValue() <= endTime
//                                )||
//
//                        (startTime >= Integer.valueOf(StringUtils.substringBefore(s,"-")).intValue() &&
//                                startTime < Integer.valueOf(StringUtils.substringAfter(s,"-")).intValue() &&
//                                    endTime >= Integer.valueOf(StringUtils.substringAfter(s,"-")).intValue()
//                                )
//
//                )
//                .collect(Collectors.toList());
//
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
//
//        subFolders.forEach(s->{
//
//            String name = simpleDateFormat.format(new Date(Integer.valueOf(StringUtils.substringBefore(s,"-")).intValue() * 1000L)) + "-" +
//            simpleDateFormat.format(new Date(Integer.valueOf(StringUtils.substringAfter(s,"-")).intValue() * 1000L));
//            File vFile = new File(destFolder+"/"+s + "/"+ name + ".mp4");
//            if(vFile == null ||!vFile.exists())
//                name = s;
//
//            Video video = new Video(downloadFolder+"/"+s + "/"+ name + ".mp4",
//                    Integer.valueOf(StringUtils.substringBefore(s,"-")),
//                    Integer.valueOf(StringUtils.substringAfter(s,"-")),
//                    downloadFolder+"/"+ s+ "/" + Constants.firstFrameImgName
//            );
//
//            video.setLength(firstFrame.getVideoTime(destFolder + "/"+s + "/"+name + ".mp4",ffmpegpath));
//
//            videos.add(video);
//
//        });
//
//        return new ResponseEntity(videos,HttpStatus.OK);
//    }

}
